#!/usr/bin/env python3
#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Simple text-based application to view basic aircraft data.
"""

import argparse
import asyncio
import contextvars
import logging
import rbfly.streams as rbs
import time
from functools import partial

import sq1090
import sq1090.assembly as sqa
import sq1090.stream as sqst

async def read_data(client: rbs.StreamsClient, stream: str) -> None:
    messages = sqst.read_messages(client, stream, offset=rbs.Offset.LAST)
    async for msg in messages:
        assert isinstance(msg, list)

        item = sqa.assembly_from_list(msg)
        sqa.assembly_set(item)

def time_delta_str(delta: float) -> str:
    if delta <= 1:
        result = '{:.3f}s'.format(delta)
    elif 1 < delta <= 60:
        result = '{:.0f}s'.format(delta)
    else:
        result = '+1min'
    return result

def to_line(ts: float, item: sqa.Assembly) -> str:
    fmt = '0x{:06x}   {:10s} {:>+12.6f} {:>+12.6f}  {:>8.1f} {:>8s}'.format
    return fmt(
        item.icao,
        item.callsign[-1],
        *item.location,
        time_delta_str(ts - item.time)
    )

async def print_table() -> None:
    """
    Print table with aircraft information.
    """
    clear = '\x1bc'
    header = '{:10s} {:10s} {:>12s} {:>12s}  {}  {}'.format(
        'ICAO', 'CALLSIGN', 'LONGITUDE', 'LATITUDE', 'ALTITUDE', 'UPDATED'
    )
    hl = '-' * len(header)
    st_view = clear + header + '\n' + hl
    while True:
        await asyncio.sleep(1)

        f = partial(to_line, time.time())
        with sqa.assembly_recent(40) as items:
            tbl = (f(v) for v in items)

        print(st_view)
        print('\n'.join(tbl))

@rbs.connection
async def view_data(client: rbs.StreamsClient, config: sq1090.Config) -> None:
    """
    Read data from Sq1090 streams and view aircraft data.
    """
    async with asyncio.TaskGroup() as tg:
        ctx = contextvars.Context()
        create_task = partial(tg.create_task, context=ctx)

        create_task(read_data(client, config.streams.stream_assembled))
        create_task(print_table())

sq1090.set_title('view')

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', default=False, action='store_true',
    help='show debug log'
)
parser.add_argument('config', help='Configuration file')

args = parser.parse_args()

level = logging.DEBUG if args.verbose else logging.WARNING
logging.basicConfig(level=level)

cfg = sq1090.read_config(args.config)
client = rbs.streams_client(cfg.streams.uri)
asyncio.run(view_data(client, cfg))

# vim: sw=4:et:ai
