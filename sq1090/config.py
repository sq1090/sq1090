#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Functions for Sq1090 application configuration.
"""

from n23.app import app_config_value, read_app_config
from n23.fn import partial
from setproctitle import setproctitle, setthreadtitle

from .types import Message, Callsign, Position, Config, ReceiverConfig, StreamsConfig

ENTITIES = {
    Message: 'message',
    Callsign: 'callsign',
    Position: 'position',
}

def read_config(fn: str) -> Config:
    cfg_val = partial(app_config_value, read_app_config(fn))
    recv = ReceiverConfig(
        cfg_val('receiver', 'host'),
        cfg_val('receiver', 'port'),
        cfg_val('receiver', 'position'),
        cfg_val('receiver', 'range'),
    )
    streams = StreamsConfig(
        cfg_val('streams', 'uri'),
        cfg_val('streams', 'offset-write-time'),
    )

    return Config(
        cfg_val('station', 'name'),
        recv,
        cfg_val('database', 'uri'),
        streams,
    )

def set_title(title: str) -> None:
    """
    Set process title.
    """
    setproctitle('sq1090: {}'.format(title))
    setthreadtitle('sq1090')

# vim: sw=4:et:ai
