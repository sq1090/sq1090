#!/usr/bin/env python3
#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Sq1090 database persistence layer.
"""

import logging
import n23
import typing as tp
from asyncpg import Connection, Pool  # type: ignore
from collections.abc import Collection, AsyncIterator
from contextlib import asynccontextmanager
from datetime import datetime
from functools import lru_cache
from n23.storage.pg.core import create_pool

from .ewkb import to_ewkb
from .types import Data, Message, Callsign, Position, Table

logger = logging.getLogger(__name__)

T = tp.TypeVar('T', bound=Data)

# NOTE: cast to float64 to use python's float, not decimal type
SQL_READ_MESSAGES = """
select cast (extract(epoch from time) as double precision) as ts, data
from message
where $1 <= time and time < $2
order by time
"""
SQL_LAST_TIME = 'select max(time) from {}'
SQL_INSERT_DATA = 'insert into {} ({}) values ({}) on conflict do nothing'

TABLE = {
    Message: Table('message', ('time', 'data')),
    Callsign: Table(
        'callsign',
        ('time', 'icao', 'category', 'type', 'callsign')
    ),
    Position: Table(
        'position',
        ('time', 'icao', 'hpl', 'rcu', 'rcv', 'location')
    ),
}

async def find_last_date(conn: Connection, entity: type) -> datetime | None:
    """
    Find last date for Sq1090 entity.
    """
    query = 'select max(time) from {}'.format(TABLE[entity].name)
    row = await conn.fetchrow(query)
    return None if (v := row['max']) is None else v

async def save_data(
        conn: Connection, entity: type[T], data: Collection[T]
    ) -> None:
    """
    Save data of given entity into its database table.
    """
    table = TABLE[entity]
    query = sql_save_data(table)

    items = ((n23.to_datetime(t), *rest) for t, *rest in data)
    await conn.executemany(query, items)
    # TODO: use copy instead as it is much faster; we need to take care
    # of duplicates though
    # await conn.copy_records_to_table(table.name, records=items, columns=table.columns)

    if __debug__:
        logger.debug(
            'data is saved: table={}, rows={}'.format(table.name, len(data))
        )

@lru_cache(maxsize=len(TABLE))
def sql_save_data(table: Table) -> str:
    columns = ', '.join(table.columns)

    items = range(1, len(table.columns) + 1)
    fields = ', '.join('${}'.format(i) for i in items)
    return SQL_INSERT_DATA.format(table.name, columns, fields)

@asynccontextmanager
async def connect(pool: Pool) -> Connection:
    """
    Connect to a database.

    Helper function to acquire a database connection, and set type codecs.

    :param pool: Database connection pool.
    """
    async with pool.acquire() as conn, conn.transaction():
        await set_type_codec(conn)
        yield conn

async def set_type_codec(conn: Connection) -> None:
    """
    Set type codec for PostgreSQL connection.

    At the moment EWKB encoding is setup to store position data.
    """
    await conn.set_type_codec(
        'geometry',
        decoder=lambda v: v,
        encoder=to_ewkb,
        format='binary'
    )

async def read_messages(
        conn: Connection,
        start_time: datetime,
        end_time: datetime,
        size: int=10 ** 5,
) -> AsyncIterator[Collection[Message]]:
    """
    Read ADS-B messages from database and iterate over the messages in
    chunks.

    :param conn: Database connection.
    :param start_time: Start time of messages (inclusive).
    :param end_time: End time of messages (exclusive).
    :param size: Chunk size.
    """
    cursor = await conn.cursor(SQL_READ_MESSAGES, start_time, end_time)
    while rows := await cursor.fetch(size, timeout=120):
        yield rows

__all__ = ['Connection', 'Pool', 'connect', 'create_pool', 'read_messages', 'save_data']

# vim: sw=4:et:ai
