#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Sq1090 data structures.
"""

import dataclasses as dtc
import typing as tp

#
# API data structures - data records
#

@dtc.dataclass(frozen=True)
class DataRecord:
    """
    Base class for a data record.
    """
    time: float
    icao: str

@dtc.dataclass(frozen=True)
class Callsign(DataRecord):
    """
    Callsign data record.
    """
    category: str
    callsign: str

@dtc.dataclass(frozen=True)
class Position(DataRecord):
    """
    Position with altitude data record.
    """
    location: tuple[float, float, float]
    hpl: float | None
    rcu: float | None
    rcv: float | None

#
# sq1090 internal, non-API data structures
#

class Message(tp.NamedTuple):
    """
    ADS-B message.
    """
    time: float
    icao: str
    typecode: int
    data: str  # hopefully bytes in the future,
               # see https://github.com/junzis/pyModeS/issues/61

class MessagePair(tp.NamedTuple):
    """
    Pair of even/odd position ADS-B messages.
    """
    even: tp.Optional[Message] = None
    odd: tp.Optional[Message] = None

class Location(tp.NamedTuple):
    """
    Airplane position.

    Low-level construct used for position calculation only.
    """
    time: float
    lon: float
    lat: float
    is_surface: bool

@dtc.dataclass(frozen=True)
class PositionCache:
    """
    Position calculation cache.

    The data stored by cache is used for position calculation and
    validatiion.

    For each airplane ICAO identifier a new position calculation entry
    should be created.

    :var time: Time of last update.
    :var pair: Even/odd position message pair.
    :var ref_pos: Reference position. Last valid position.
    :var track_start: Position starting track of an airplane.
    """
    time: float = 0.0
    pair: MessagePair = dtc.field(default_factory=MessagePair)
    ref_pos: tp.Optional[Location] = None
    track_start: tp.Optional[Location] = None

ParserFunction = tp.Callable[[Message], tp.Optional[DataRecord]]

# vim: sw=4:et:ai
