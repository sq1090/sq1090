#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Sq1090 high-level data processing functions.
"""

import asyncio
import logging
import numpy as np
import typing as tp
import vmodes
from vmodes.util import hstack, vstack
from collections.abc import Collection, Iterable
from contextvars import ContextVar

from sq1090.types import Callsign, Position

logger = logging.getLogger(__name__)

CTX_POS_DECODER = ContextVar[vmodes.PosDecoderCtx]('CTX_POS_DECODER')

class AircraftData(tp.NamedTuple):
    """
    Basic aircraft data.

    :var time: Time of receiving of Mode S or ADS-B message.
    :var message: Mode S and ADS-B messages.
    :var df: Downlink format information.
    :var typecode: Type code information.
    :var icao: ICAO address.
    """
    time: vmodes.Time
    message: vmodes.Message
    df: vmodes.DownlinkFormat
    typecode: vmodes.TypeCode
    icao: vmodes.Icao

T = tp.TypeVar('T', Callsign, Position)
Ts = tp.TypeVarTuple('Ts')
Queue: tp.TypeAlias = asyncio.Queue[AircraftData | None]
Processor: tp.TypeAlias = tp.Callable[[AircraftData], list[T]]

def aircraft_data(data: Collection[tuple[float, bytes]]) -> AircraftData:
    ts_data, msg_data = zip(*data)
    ts = np.array(ts_data, dtype=np.float64)
    messages = vmodes.message_array(msg_data)
    assert len(ts) == messages.shape[0] == len(data)

    #data = [tuple(v) for v in data]
    #datav = np.array(data, dtype=[('time', np.double), ('data', '|S14')])
    #ts = datav['time']
    #messages = np.array(datav['data']).view(np.uint8).reshape(-1, 14)

    df = vmodes.df(messages)
    typecode = vmodes.typecode(messages, df)
    icao = vmodes.icao(messages, df)

    return AircraftData(ts, messages, df, typecode, icao)

def callsign(ad: AircraftData) -> list[Callsign]:
    """
    Decode aircraft call sign data.
    """
    category = vmodes.category(ad.message, ad.typecode)
    callsign = vmodes.callsign(ad.message, ad.typecode)

    idx = vmodes.data_index(category) & vmodes.data_index(callsign)
    ts = ad.time[idx]
    icao = ad.icao[idx]
    cat = category[idx, 0]
    cat_type = category[idx, 1]
    callsign = callsign[idx]

    items = list(np_zip(ts, icao, cat, cat_type, callsign))  # type: ignore
    return items  # type: ignore

def position(ad: AircraftData) -> list[Position]:
    """
    Decode aircraft position data.

    .. seealso: `create_pos_context`
    """
    ctx = CTX_POS_DECODER.get()
    ctx, records = decode_positions(ctx, ad)
    CTX_POS_DECODER.set(ctx)
    return records

def decode_positions(
        ctx: vmodes.PosDecoderCtx, ad: AircraftData
) -> tuple[vmodes.PosDecoderCtx, list[Position]]:

    pd = ctx.carry_over.data
    message = vstack(pd.data, ad.message)
    ts = hstack(pd.time, ad.time)
    icao = hstack(pd.icao, ad.icao)
    typecode = hstack(pd.typecode, ad.typecode)

    pos_data = vmodes.position(ctx, ad.message, ad.time, ad.icao, ad.typecode)
    position = vstack(pos_data.prev_position, pos_data.position)
    nuc_p = vmodes.nuc_p(message, typecode)
    altitude = vmodes.altitude(message, typecode)

    idx = vmodes.data_index(nuc_p) \
        & vmodes.data_index(position) \
        & vmodes.data_index(altitude)

    ts = ts[idx]
    icao = icao[idx]
    nuc_p = nuc_p[idx, :]
    position = position[idx, :]
    altitude = altitude[idx]

    items = np_zip(ts, icao, nuc_p, position, altitude)  # type: ignore
    records: list[Position] = [
        (t, i, *n, (*p, a)) for t, i, n, p, a in items  # type: ignore
    ]
    return pos_data.ctx, records

def create_pos_context(
        longitude: float, latitude: float, range: float,
) -> None:
    """
    Create position decoding context.

    :param longitude: Longitude of position of aircraft message receiver.
    :param latitude: Latitude of position of aircraft message receiver.
    :param range: Range of aircraft message receiver.
    """
    receiver = vmodes.Receiver(longitude, latitude, range)
    CTX_POS_DECODER.set(vmodes.PosDecoderCtx(receiver))
    logger.info('position decoding context is set')

def np_zip(*arrays: np.ndarray[tp.Any, *Ts]) -> Iterable[tuple[*Ts]]:  # type: ignore
    return zip(*(a.tolist() for a in arrays))

# vim: sw=4:et:ai
