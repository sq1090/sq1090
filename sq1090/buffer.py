#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Data buffer structure with limited size.
"""

import asyncio
import typing as tp
from collections import deque
from collections.abc import Collection, AsyncIterator
from contextlib import asynccontextmanager

T = tp.TypeVar('T')

class DataBuffer(tp.Generic[T]):
    """
    Data buffer structure with limited size.
    """
    def __init__(self, max_size: int) -> None:
        self._cond = asyncio.Condition()
        self._max_size = max_size
        self._data = deque[T]()

    async def put(self, item: T) -> None:
        """
        Add an item to data buffer.

        The method blocks when data buffer is full.
        """
        cond = self._cond
        async with cond:
            await cond.wait_for(lambda: len(self._data) < self._max_size)
            self._data.append(item)

    @asynccontextmanager
    async def get_all(self) -> AsyncIterator[Collection[T]]:
        """
        Get all items stored in data buffer.

        The method is a context manager. On exit, it clears the data stored
        in the buffer and unblocks coroutines blocked by
        :py:method:`DataBuffer.put` method.
        """
        cond = self._cond
        async with self._cond:
            yield self._data
            self._data.clear()
            cond.notify_all()

    def empty(self) -> bool:
        """
        Check if the data buffer is empty.
        """
        return len(self._data) == 0

# vim: sw=4:et:ai
