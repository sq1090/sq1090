#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Sq1090 RabbitMQ Streams utilities.
"""

import dataclasses as dtc
import logging
import rbfly.streams as rbs
import time
import typing as tp
from collections.abc import Awaitable, AsyncIterator
from n23.fn import partial

import sq1090.db as sqdb
from .buffer import DataBuffer
from .types import VERSION_DATA, Message, MessageMeta, Config

logger = logging.getLogger(__name__)

# how old aircraft messages can be to validate current position
# of an aircraft; based on
#
# - vmodes/vmodes/decoder/_position.pyx
# - TIME_CUTOFF_SRF = TIME_NEXT_MESSAGE_PAIR + 2 * TIME_SURFACE_MSG_PAIR
N_TIME_OFFSET = 130

OffsetWriter: tp.TypeAlias = tp.Callable[[float, int], Awaitable[None]]

@dtc.dataclass(frozen=True)
class MessageOffsetWriteInfo:
    """
    Data for writing offset to aircraft message RabbitMQ stream.

    :param client: RabbitMQ client.
    :param stream: Name of aircraft message RabbitMQ stream.
    :param offset_ref: Offset reference.
    :param frequency: Frequency at which stream offset is written (in
        seconds).
    """
    client: rbs.StreamsClient
    stream: str
    offset_ref: str
    frequency: int

async def read_stream_offset(conn: sqdb.Connection, entity: type) -> rbs.Offset:
    value = await sqdb.find_last_date(conn, entity)
    if value is None:
        offset = rbs.Offset.offset(0)
    else:
        offset = rbs.Offset.timestamp(value.timestamp())

    logger.info(
        'entity offset read: entity={}, offset={}'
        .format(entity.__name__.lower(), offset)
    )
    return offset

def ctor_create_message(
        config: Config
) -> tp.Callable[[rbs.AMQPBody], rbs.MessageCtx]:
    """
    Create function to create stream messages.
    """
    properties: rbs.AMQPAppProperties = {
        MessageMeta.VERSION.value: VERSION_DATA,
        MessageMeta.STATION.value: config.name,
    }
    return partial(create_message, properties=properties)

def create_message(
        data: rbs.AMQPBody, properties: rbs.AMQPAppProperties
) -> rbs.MessageCtx:
    """
    Create AMQP message to be sent to a RabbitMQ stream.

    Assign version of message format.

    :param data: Message body.
    :param properties: Message metadata (AMQP application properties).
    """
    ctx = rbs.stream_message_ctx(data, app_properties=properties)
    return ctx

async def read_messages(
        client: rbs.StreamsClient,
        stream: str,
        *,
        offset: rbs.Offset=rbs.Offset.NEXT,
) -> AsyncIterator[rbs.AMQPBody]:
    """
    Read message from RabbitMQ stream, and verify its metadata.

    :param client: RabbitMQ Streams client.
    :param stream: RabbitMQ stream name.
    :param offset: RabbitMQ stream offset value.
    """
    async for msg in client.subscribe(stream, offset=offset):
        # TODO: this needs to be more sophisticated
        ctx = rbs.get_message_ctx()
        assert ctx.app_properties[MessageMeta.VERSION.value] == VERSION_DATA
        assert MessageMeta.STATION.value in ctx.app_properties

        yield msg

async def read_into_buffer(
        client: rbs.StreamsClient,
        stream: str,
        offset: rbs.Offset,
        buffer: DataBuffer[Message],
) -> None:
    """
    Read data from RabbitMQ stream and put the data into the buffer.

    :param client: RabbitMQ Streams client.
    :param stream: RabbitMQ stream name.
    :param offset: RabbitMQ stream offset value.
    :param buffer: Data buffer.
    """
    async for msg in read_messages(client, stream, offset=offset):
        await buffer.put(msg)  # type: ignore

#
# writing offset to message stream, to restart processing from that offset
#
def message_stream_offset_ctor(
        info: MessageOffsetWriteInfo,
        /,
        delta: int=N_TIME_OFFSET, store: list[int] | None=None
) -> OffsetWriter:
    """
    Create coroutine to write offset of aircraft data RabbitMQ stream.

    The offset is used by Sq1090 data processing application when the
    application is started. The processing of data starts from the written
    stream offset.

    Important notes

    1. The stream offset is written periodically
       (py:attr:`~MessageOffsetWriteInfo.frequency`).
    2. The written offset is at least 110 seconds old (see `N_TIME_OFFSET`).
    3. Above is required for positions calculation. Most recent positions
       need to be validated with position data from the past.

    """
    if store is None:
        store = [0] * delta
    assert store is not None and len(store) == delta

    # when the offset was stored in message stream
    last_write = time.time()
    # value of last offset written
    last_offfset = 0

    async def writer(ts: float, offset: int) -> None:
        nonlocal last_write, last_offfset, store
        offset = offset_to_write(store, ts, offset)

        # ensure written offset value is stricly monotonic over time;
        # when little of aircraft messages are arriving, then `store` is
        # sparsely populated, and offset to write might be non-monotonic;
        # avoid the problem by checking with `last_offfset`
        if offset > last_offfset:
            last_write = await write_offset(info, last_write, offset)
            last_offfset = offset

    return writer

def offset_to_write(store: list[int], ts: float, offset: int) -> int:
    n = len(store)
    k = int(ts) % n
    store[k] = offset
    return store[(k + 1) % n]

async def write_offset(
        info: MessageOffsetWriteInfo,
        last_write: float,
        offset: int,
) -> float:
    """
    Write offset value for a stream.

    :param info: RabbitMQ message stream information.
    :param last_write: When offset was written last time.
    :param offset: RabbitMQ stream offset value to write.
    """
    now = time.time()
    if now - last_write > info.frequency:
        await info.client.write_offset(info.stream, info.offset_ref, offset)
        last_write = now

    return last_write

# vim: sw=4:et:ai
