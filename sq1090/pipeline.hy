;
; Sq1090 - high-level API to parse and publish ADS-B messages
;
; Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU Affero General Public License as
; published by the Free Software Foundation, either version 3 of the
; License, or (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU Affero General Public License for more details.
;
; You should have received a copy of the GNU Affero General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;

(import operator [attrgetter]
        sq1090.stream [ctor-create-message])

(require n23.pipeline [n23->])

(defn ctor-stream-adsb-message [cfg]
    (setv getter (attrgetter "time" "value"))
    (setv create-message (ctor-create-message cfg))
    (n23-> getter create-message))

; vim: sw=4:et:ai
