#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Definition of types and type aliases for Sq1090 application.
"""

import dataclasses as dtc
import enum
import typing as tp

VERSION_DATA = 1

Data: tp.TypeAlias = tp.NamedTuple

class MessageMeta(enum.Enum):
    """
    Metadata of each message published to a stream.
    """
    VERSION = '.v'
    STATION = '.st'

@dtc.dataclass(frozen=True)
class ReceiverConfig:
    """
    Receiver of ADS-B messages.

    :param host: Hostname of receiver.
    :param port: TCP/IP port of the receiver.
    :param position: Longitude and latitude of the receiver.
    :param range: Range of the receiver in meters.
    """
    host: str
    port: int
    position: tuple[float, float]
    range: int

@dtc.dataclass(frozen=True)
class StreamsConfig:
    """
    Streams configuration.

    :param uri: Streams broker URI.
    :param offset_write_time: How often to update streams offset reference.
    """
    uri: str
    offset_write_time: int = 120

    message_buffer_size: int = 10 ** 6
    message_flush_time = 0.1
    # this should not breach default 1 mb rabbitmq streams buffer size
    batch_size: int = 10000

    stream_message: str = 'sq1090.message'
    stream_callsign: str = 'sq1090.callsign'
    stream_position: str = 'sq1090.position'
    stream_assembled: str = 'sq1090.assembled'

@dtc.dataclass(frozen=True)
class Config:
    """
    Configuration of Sq1090 application.

    :param name: Station name.
    :param receiver: ADS-B message receiver information.
    :param db_uri: Database connection URI.
    :param streams: Streams configuration.
    """
    name: str
    receiver: ReceiverConfig
    db_uri: str
    streams: StreamsConfig

class Message(tp.NamedTuple):
    time: float
    data: bytes

class Callsign(tp.NamedTuple):
    time: float
    icao: int
    category: int
    type: int
    callsign: str

class Position(tp.NamedTuple):
    time: float
    icao: int
    hpl: float
    rcu: int
    rcv: int
    location: tuple[float, float, float]

class Assembly(tp.NamedTuple):
    time: float
    icao: int
    callsign: tuple[int, int, str]=(0, 0, '')
    location: tuple[float, float, float]=(0, 0, 0)
    nuc: tuple[float, int, int]=(0, 0, 0)

@dtc.dataclass(frozen=True)
class Table:
    """
    Database table description.

    :var name: Database table name.
    :var columns: Database table columns.
    """
    name: str
    columns: tuple[str, ...]

AssemblyCollection: tp.TypeAlias = dict[int, Assembly]

# vim: sw=4:et:ai
