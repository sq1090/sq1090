#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Functions to combine Sq1090 aircraft data into one view.

The aircraft data is combined into :py:class:`sq1090.types.Assembly`
objects using

- callsign data
- position data

An assembly simplifies use of Sq1090 aircraft data.
"""

import contextvars
import operator as op
import time
import typing as tp
from collections.abc import Iterable, Iterator
from contextlib import contextmanager
from itertools import islice

from .types import Assembly, AssemblyCollection, Callsign, Position

CTX_ASSEMBLY = contextvars.ContextVar[AssemblyCollection](
    'CTX_ASSEMBLY', default={}
)
CTX_ASSEMBLY_UPDATED = contextvars.ContextVar[set[int]](
    'CTX_ASSEMBLY_UPDATED', default=set()
)

Ta = tp.TypeVar('Ta', Callsign, Position)
AssemblyIterCtx: tp.TypeAlias = Iterator[Iterable[Assembly]]

op_time = op.attrgetter('time')
op_category = op.attrgetter('category', 'type', 'callsign')
op_nuc = op.attrgetter('hpl', 'rcu', 'rcv')

@contextmanager
def assembly_updated() -> AssemblyIterCtx:
    """
    Return collection of assembly objects.

    Return only objects marked as updated with `assembly_set` function.

    On exit, the returned assembly objects are no longer marked as updated.

    .. seealso:: :py:func:`~assembly_set`
    """
    updated = CTX_ASSEMBLY_UPDATED.get()
    coll = CTX_ASSEMBLY.get()
    items = (coll[i] for i in updated)
    yield sorted(items, key=op_time)
    _clean(coll)
    updated.clear()

@contextmanager
def assembly_recent(n: int) -> AssemblyIterCtx:
    """
    Return collection of most recent assembly objects.

    The collection is sorted by time, in descending order.

    :param n: Number of objects to return.
    """
    coll = CTX_ASSEMBLY.get()
    # TODO: make the number of returned items configurable
    items = islice(sorted(coll.values(), key=op_time, reverse=True), n)
    yield items
    _clean(coll)

def assembly_get(time: float, icao: int) -> Assembly:
    coll = CTX_ASSEMBLY.get()
    item = coll.get(icao)
    if item is None:
        item = Assembly(time, icao)
    return item

def assembly_set(item: Assembly) -> None:
    """
    Add assembly to the collection of assembly objects.

    Mark the assembly as updated.

    .. seealso:: :py:func:`~assembly_updated`
    """
    CTX_ASSEMBLY.get()[item.icao] = item
    CTX_ASSEMBLY_UPDATED.get().add(item.icao)

def assembly_update(item: Assembly, msg: Ta) -> Assembly:
    """
    Update assembly object with position or callsign data.
    """
    time = msg.time
    match msg:
        case Callsign():
            new_item = item._replace(time=time, callsign=op_category(msg))
        case Position():
            new_item = item._replace(
                time=time, location=msg.location, nuc=op_nuc(msg)
            )
    return new_item

def assembly_from_list(msg: list[tp.Any]) -> Assembly:
    """
    Convert list representation of an assembly to assembly object.

    The list representation of an assembly object is received from
    a RabbitMQ stream.
    """
    assert isinstance(msg, list), type(msg)
    time, icao, cs, loc, nuc = msg
    return Assembly(time, icao, tuple(cs), tuple(loc), tuple(nuc))

def _clean(coll: AssemblyCollection) -> None:
    now = time.time()
    items = ((i, v) for i, v in coll.items() if now - v.time < 600)
    CTX_ASSEMBLY.set(dict(items))

# vim: sw=4:et:ai
