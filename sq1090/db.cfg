(defmacro icao-table [name columns #* rest]
  `(quote (entity ~name
     (partition-by icao)
     (columns (icao int4)
              ~@columns)
     ~@rest)))

(storage
  (version "1.0.0")
  (role sq1090)
  (extensions (timescaledb postgis)))

(entities
  ; raw message table
  (entity message
    (columns (data bytea)))

  ; aircraft callsign
  ~(icao-table callsign
    ((category smallint)
     (type smallint)
     (callsign "varchar(8)")))

  ; position with altitude
  ~(icao-table position
    ((location "geometry(PointZ, 4326)")
     (hpl "numeric(6, 1)" null)
     (rcu "numeric(5, 0)" null)
     (rcv "numeric(2, 0)" null))))

(sql #[[
create materialized view callsign_15min with (timescaledb.continuous) as
select time_bucket('15 minutes', time) as time_bucket,
    icao,
    first(callsign, time) as callsign,
    category
from callsign
group by time_bucket, icao, category;

create materialized view message_1h 
with (timescaledb.continuous)
as
select
    time_bucket('1h', time) as time,
    least(24, cast(get_byte(data, 0)::bit(8) >> 3 as integer)) as df,
    count(*) as count
from message
group by 1, 2;

alter materialized view message_1h set (timescaledb.materialized_only=false);

select add_continuous_aggregate_policy(
  'message_1h',
  start_offset => INTERVAL '1 day',
  end_offset => INTERVAL '1 hour',
  schedule_interval => INTERVAL '1 day'
);

create table if not exists downlink_format (
    df smallint not null,
    name varchar(50) not null,
    primary key(df)
);

insert into downlink_format (df, name) values
    (0, 'Short ACAS'),
    (1, 'Reserved'),
    (2, 'Reserved'),
    (3, 'Reserved'),
    (4, 'Altitude (surveillance)'),
    (5, 'Identity (surveillance)'),
    (6, 'Reserved'),
    (7, 'Reserved'),
    (8, 'Reserved'),
    (9, 'Reserved'),
    (10, 'Reserved'),
    (11, 'All-call reply'),
    (12, 'Reserved'),
    (13, 'Reserved'),
    (14, 'Reserved'),
    (15, 'Reserved'),
    (16, 'Long ACAS'),
    (17, 'ES'),
    (18, 'Non-transponder ES'),
    (19, 'Military ES'),
    (20, 'Altitude (Comm-B)'),
    (21, 'Identity (Comm-B)'),
    (22, 'Reserved'),
    (23, 'Reserved'),
    (24, 'ELM');

create table if not exists category (
    category smallint,
    type smallint,
    name varchar(50) not null,
    primary key (category, type)
);

insert into category (category, type, name) values
    (0, 4, 'No information'),
    (1, 4, 'Light (< 15 500 lb)'),
    (2, 4, 'Medium 1 (15 500 to 75 000 lb)'),
    (3, 4, 'Medium 2 (75 000 to 300 000 lb)'),
    (4, 4, 'High vortex aircraft'),
    (5, 4, 'Heavy (> 300 000 lb)'),
    (6, 4, 'High performance (> 5 G and > 400 kn)'),
    (7, 4, 'Rotorcraft'),
    (0, 3, 'No information'),
    (1, 3, 'Glider/sailplane'),
    (2, 3, 'Lighter-than-air'),
    (3, 3, 'Parachutist, skydiver'),
    (4, 3, 'Ultralight, glider'),
    (5, 3, 'Reserved'),
    (6, 3, 'Unmanned aerial vehicle'),
    (7, 3, 'Space/transatmospheric vehicle'),
    (0, 2, 'No information'),
    (1, 2, 'Surface emergency vehicle'),
    (2, 2, 'Surface service vehicle'),
    (3, 2, 'Fixed obstruction'),
    (4, 2, 'Reserved'),
    (5, 2, 'Reserved'),
    (6, 2, 'Reserved'),
    (7, 2, 'Reserved'),
    (0, 1, 'Reserved'),
    (1, 1, 'Reserved'),
    (2, 1, 'Reserved'),
    (3, 1, 'Reserved'),
    (4, 1, 'Reserved'),
    (5, 1, 'Reserved'),
    (6, 1, 'Reserved'),
    (7, 1, 'Reserved');
]])

; vim: sw=2:et:ai:syn=lisp
