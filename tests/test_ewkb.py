#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
EWKB conversion functions.
"""

from sq1090.ewkb import to_ewkb

def test_to_ewkb() -> None:
    """
    Test conversion of a position to WKB representation.
    """
    # st_setsrid(st_makepoint(-6.1, 53.2, 132), 4326)
    expected = b'\x01\x01\x00\x00\xa0\xe6\x10\x00\x00ffffff\x18\xc0\x9a\x99\x99\x99\x99\x99J@\x00\x00\x00\x00\x00\x80`@'
    assert to_ewkb((-6.1, 53.2, 132)) == expected

# vim: sw=4:et:ai
