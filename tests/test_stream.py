#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Unit tests for Sq1090 functions related to RabbitMQ Streams.
"""

from sq1090.stream import ctor_create_message, message_stream_offset_ctor, \
    offset_to_write, MessageOffsetWriteInfo
from sq1090.types import Config, ReceiverConfig, StreamsConfig, VERSION_DATA

import pytest
from unittest import mock

def test_create_message() -> None:
    """
    Test create message context for a stream.
    """
    config = Config(
        'st-01',
        ReceiverConfig('localhost', 1, (1, 1), 1),
        'uri',
        StreamsConfig('uri'),
    )
    create_message = ctor_create_message(config)
    ctx = create_message(('a', 1, 'b'))
    assert ctx.body == ('a', 1, 'b')
    assert ctx.app_properties == {'.v': VERSION_DATA, '.st': 'st-01'}

@pytest.mark.asyncio
async def test_stream_message_offset() -> None:
    delta = 4
    store = [0] * delta
    info = MessageOffsetWriteInfo(
        mock.AsyncMock(), 'messages', 'sq1090/processor', 5
    )
    write_offset = message_stream_offset_ctor(info, delta=delta, store=store)
    await write_offset(1.1, 13)
    assert store == [0, 13, 0, 0]

#@pytest.mark.parametrized('store, ts, offset, expected_offset
def test_offset_to_write() -> None:
    store = [400, 0, 200]
    offset = offset_to_write(store, 4.1, 600)
    assert offset == 200
    assert store[1] == 600

def test_offset_to_write_edge() -> None:
    store = [2, 20, 0]
    offset = offset_to_write(store, 5.1, 200)
    assert offset == 2
    assert store[2] == 200

# vim: sw=4:et:ai
