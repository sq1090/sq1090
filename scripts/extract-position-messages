#!/usr/bin/env python
#
# Sq1090 - high-level API to parse and publish ADS-B messages
#
# Copyright (C) 2018-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Extract position ADS-B messages.

The information to be extracted

- time
- type code
- icao
- is even (0) or odd (1)
- NUCp (hpl, rcu, rcv)
- altitude
- message

The data is sent to standard output in CSV format.
"""

import argparse
import asyncio
import binascii
import csv
import gzip
import pyModeS as pms
import sys

from n23.storage.pg.core import to_db_uri, connect

COLUMNS = (
    'time', 'typecode', 'icao', 'is_even', 'hpl', 'rcu', 'rcv',
    'altitude', 'data',
)

SQL_DATA = """
select time, data
from message
where get_byte(data, 0) = 141
order by time
"""

def extract_row(ts, msg, tc):
    tc = pms.adsb.typecode(msg)
    icao = pms.adsb.icao(msg)
    oe_flag = pms.adsb.oe_flag(msg)
    hpl, rcu, rcv = pms.adsb.nuc_p(msg)
    alt = pms.adsb.altitude(msg)

    return (ts.isoformat(), tc, icao, oe_flag, hpl, rcu, rcv, alt, msg)

async def extract_data(db_uri):
    async with connect(db_uri) as conn:
        data = conn.cursor(SQL_DATA, prefetch=10 ** 6)
        data = (
            (ts, binascii.hexlify(msg).decode())
            async for ts, msg in data
        )
        data = ((ts, msg, pms.adsb.typecode(msg)) async for ts, msg in data)
        data = (
            extract_row(ts, msg, tc)
            async for ts, msg, tc in data
            if 5 <= tc <= 18 or 20 <= tc <= 22
        )

        writer = csv.writer(sys.stdout)
        writer.writerow(COLUMNS)
        async for row in data:
            writer.writerow(row)

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', action='store_true', default=False,
    help='Explain what is being done',
)
parser.add_argument(
    'db', help='Database name or database connection string'
)

args = parser.parse_args()
db_uri = to_db_uri(args.db)

asyncio.run(extract_data(db_uri))

# vim: sw=4:et:ai
