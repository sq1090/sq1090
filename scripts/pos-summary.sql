select
    cast(time_bucket('1 year', time) as date) as start,
    round(cast(min(st_x(location)) as numeric), 6) as min_lon,
    round(cast(max(st_x(location)) as numeric), 6) as max_lon,
    round(cast(min(st_y(location)) as numeric), 6) as min_lat,
    round(cast(max(st_y(location)) as numeric), 6) as max_lat,
    round(cast(min(st_z(location)) as numeric), 1) as min_alt,
    round(cast(max(st_z(location)) as numeric), 1) as max_alt,
    count(*)
from position
group by start;
