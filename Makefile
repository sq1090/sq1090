include .pymake.mk

SCRIPTS = \
	bin/sq1090-assembler \
	bin/sq1090-message \
	bin/sq1090-database \
	bin/sq1090-processor \
	bin/sq1090-reprocess \
	bin/sq1090-view \
	bin/sq1090-plot-range \
	scripts/sq1090-message-resend \
	scripts/sq1090-message-sender

DOC_DEST = wrobell@dcmod.org:~/public_html/$(PROJECT)
